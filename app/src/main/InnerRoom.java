/**
 * Classe responsável por guardar e manipular
 * informações da sala interna (a ser criada pelo usuário);
 */
public class InnerRoom {
    private String uuid;
    private List<User> members;
    private Room parent;

    /**
     * Opcionalmente pode se criar mais atributos;
     *
     * @param uuid  Unique Universal IDentification, string gerada pela classe UUID para identificar a sala interna forma única.
     * @param members Lista dos usuários que estão na sala interna, objetos tipo User.
     * @param parent Objeto Room onde a sala interna foi criada;
     */
    public Room(String uuid, List<User> members, Room parent) {
        
    }
}