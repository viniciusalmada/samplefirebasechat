/**
 * Classe responsável por guardar e manipular
 * informações de usuários;
 */
public class User {
    private String name;
    private String email;
    private String uid;

    /**
     * Opcionalmente pode se criar mais atributos como
     * idade, sexo, etc;;
     *
     * @param name Nome e sobrenome do usuário
     * @param email Email do usuário
     * @param uid  <strong>U</strong>ser <strong>ID</strong>entification
     *             String gerada pelo FirebaseAuth para identificar cada usuário
     *             de forma única.
     */
    public User(String name, String email, String uid) {
        
    }
}



/**
 * Classe responsável por guardar e manipular
 * informações da sala (a ser criada pelo usuário);
 */
public class Room {
    private String name;
    private String uuid;
    private List<User> members;
    private Location location;

    /**
     * Opcionalmente pode se criar mais atributos;
     *
     * @param name Nome da sala.
     * @param uuid  Unique Universal IDentification, string gerada pela classe UUID para identificar a sala de forma única.
     * @param members Lista dos usuários que estão na sala, objetos tipo User.
     * @param location Localização da sala, latitude e longitude;
     */
    public Room(String name, String uuid, List<User> members, Location location) {
        
    }
}



/**
 * Classe responsável por guardar e manipular
 * informações da sala (a ser criada pelo usuário);
 */
public class Message {
    private String message;
    private String uidUser;
    private long timestamp;
    private String nameUser;

    /**
     * Opcionalmente pode se criar mais atributos;
     *
     * @param message Nome da sala.
     * @param uidUser  Uid do usuário que enviou a mensagem (estando já dentro da sala)
     * @param timestamp número retornado pelo método getCurrentTimeMilis() para ordenar as mensagens
     * @param nameUser Nome do usuário que enviou a mensagem
     */
    public Room(String message, String uidUser, long timestamp, String nameUser) {
       
    }
}



/**
 * Classe responsável por guardar e manipular
 * informações da sala interna (a ser criada pelo usuário);
 */
public class InnerRoom {
    private String uuid;
    private List<User> members;
    private Room parent;

    /**
     * Opcionalmente pode se criar mais atributos;
     *
     * @param uuid  Unique Universal IDentification, string gerada pela classe UUID para identificar a sala interna forma única.
     * @param members Lista dos usuários que estão na sala interna, objetos tipo User.
     * @param parent Objeto Room onde a sala interna foi criada;
     */
    public Room(String uuid, List<User> members, Room parent) {
        
    }
}