package br.com.viniciusalmada.samplechatfirebase;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import br.com.viniciusalmada.samplechatfirebase.adapter.ListFriendsToCreateRoomAdapter;
import br.com.viniciusalmada.samplechatfirebase.adapter.MessagesRoomAdapter;
import br.com.viniciusalmada.samplechatfirebase.domain.InnerRoom;
import br.com.viniciusalmada.samplechatfirebase.domain.Message;
import br.com.viniciusalmada.samplechatfirebase.domain.Room;
import br.com.viniciusalmada.samplechatfirebase.domain.User;

/**
 * Created by vinicius-almada on 27/12/16.
 */

public class RoomActivity extends AppCompatActivity {

    public static final String TAG = "RoomActivity";
    private String uuidRoom = "";
    private String uuidInnerRoom = "";
    private boolean isRoomParentClicked = true;
    private Room roomParent = new Room();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        Intent it = getIntent();
        uuidRoom = it.getStringExtra("room");
        init();
    }

    private void init() throws NullPointerException{

        // Setar o nome da sala
        DatabaseReference roomRef = FirebaseDatabase.getInstance().getReference().child("rooms").child(uuidRoom);
        roomRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Room room = dataSnapshot.getValue(Room.class);
                ((TextView) findViewById(R.id.name)).setText(room.getName());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Setar o as mensagens da sala
        DatabaseReference messagesRef = FirebaseDatabase.getInstance().getReference().child("messages").child(uuidRoom);
        messagesRef.orderByChild("timestamp").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Message> listMessages = new ArrayList<>();
                Iterable<DataSnapshot> it = dataSnapshot.getChildren();
                for (int i = 0; i < dataSnapshot.getChildrenCount(); i++) {
                    listMessages.add(it.iterator().next().getValue(Message.class));
                }

                MessagesRoomAdapter adapter = new MessagesRoomAdapter(RoomActivity.this, listMessages);
                LinearLayoutManager llm = new LinearLayoutManager(RoomActivity.this, LinearLayoutManager.VERTICAL, false);
                RecyclerView rv = (RecyclerView) findViewById(R.id.rvmsgs);

                rv.setAdapter(adapter);
                rv.setLayoutManager(llm);
                rv.scrollToPosition(rv.getAdapter().getItemCount() - 1);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Setar as innerRooms existentes
        DatabaseReference innerRoomsRef = FirebaseDatabase.getInstance().getReference().child("inner_rooms");
        innerRoomsRef.child(uuidRoom).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final List<InnerRoom> innerRoomList = new ArrayList<>();
                Iterable<DataSnapshot> it = dataSnapshot.getChildren();
                for (int i = 0; i < dataSnapshot.getChildrenCount(); i++) {
                    InnerRoom innerRoom =  it.iterator().next().getValue(InnerRoom.class);
                    if (innerRoom.containsUser(MainActivity.mUser.getUid())){
                        innerRoomList.add(innerRoom);
                    }
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(RoomActivity.this, android.R.layout.simple_spinner_item, getStringsMembers(innerRoomList));
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                Spinner spinnerInnerRooms = (Spinner) findViewById(R.id.sprooms);
                spinnerInnerRooms.setAdapter(adapter);
                spinnerInnerRooms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        uuidInnerRoom = innerRoomList.get(position).getUUID();

                        /*DatabaseReference innerRoomRef*/

                        DatabaseReference messagesInnerRoomsRef = FirebaseDatabase.getInstance().getReference().child("messages").child(uuidInnerRoom);
                        messagesInnerRoomsRef.orderByChild("timestamp").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                List<Message> listMessages = new ArrayList<>();
                                Iterable<DataSnapshot> it = dataSnapshot.getChildren();
                                for (int i = 0; i < dataSnapshot.getChildrenCount(); i++) {
                                    listMessages.add(it.iterator().next().getValue(Message.class));
                                }

                                MessagesRoomAdapter adapter = new MessagesRoomAdapter(RoomActivity.this, listMessages);
                                LinearLayoutManager llm = new LinearLayoutManager(RoomActivity.this, LinearLayoutManager.VERTICAL, false);
                                RecyclerView rv = (RecyclerView) findViewById(R.id.rvinner);

                                rv.setAdapter(adapter);
                                rv.setLayoutManager(llm);
                                rv.scrollToPosition(rv.getAdapter().getItemCount() - 1);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private String[] getStringsMembers(List<InnerRoom> innerRooms) {
        String[] aux = new String[innerRooms.size()];
        for (int i = 0; i < aux.length; i++) {
            aux[i] = innerRooms.get(i).getMembersName();
        }
        return aux;
    }

    public void sendMessage(View view) {
        Message message = new Message();
        message.setMessage(((EditText) findViewById(R.id.text)).getText().toString());
        message.setUidUser(MainActivity.mUser.getUid());
        message.setTimestamp(System.currentTimeMillis());
        message.setNameUser(MainActivity.mUser.getName());
        if (isRoomParentClicked) {
            message.saveOnFirebase(uuidRoom);
        } else {
            message.saveOnFirebase(uuidInnerRoom);
        }
    }

    public void createInnerRoom(View view) {
        View v = getLayoutInflater().inflate(R.layout.dialog_create_inner_room, null, false);
        final Dialog dialog = new Dialog(this);
        final ListView friendsList = (ListView) v.findViewById(R.id.name);
        DatabaseReference currentRoomRef = FirebaseDatabase.getInstance().getReference().child("rooms").child(uuidRoom);
        currentRoomRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                roomParent = dataSnapshot.getValue(Room.class);
//                membersList = roomParent.getMembers();
                ArrayAdapter<String> adapter = new ArrayAdapter<>(RoomActivity.this, android.R.layout.simple_list_item_multiple_choice, getStrings(roomParent.getMembers(MainActivity.mUser.getUid(), false)));
                friendsList.setAdapter(adapter);
                friendsList.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        (v.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SparseBooleanArray checkedsFriends = friendsList.getCheckedItemPositions();
                List<User> selectedItems = new ArrayList<>();
                for (int i = 0; i < checkedsFriends.size(); i++) {
                    int pos = checkedsFriends.keyAt(i);
                    if (checkedsFriends.valueAt(i))
                        selectedItems.add(roomParent.getMembers(MainActivity.mUser.getUid(), true).get(pos));
                }
                selectedItems.add(MainActivity.mUser);
                uuidInnerRoom = UUID.randomUUID().toString().replaceAll("-", "");
                InnerRoom room = new InnerRoom();
                room.setName("inner");
                room.setUUID(uuidInnerRoom);
                room.setMembers(selectedItems);
                room.setRoomParent(roomParent);
                room.saveOnFirebase();
                dialog.dismiss();
            }
        });
        dialog.setContentView(v);
        dialog.show();
    }

    private String[] getStrings(List<User> list) {
        String[] aux = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            aux[i] = list.get(i).getName();
        }
        return aux;
    }

    public void chooseChat(View view) {
        switch (view.getId()){
            case R.id.rb_inner:
                isRoomParentClicked = false;
                break;
            case R.id.rb_room:
                isRoomParentClicked = true;
                break;
        }
    }
}
