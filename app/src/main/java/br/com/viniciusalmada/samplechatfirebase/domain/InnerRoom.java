package br.com.viniciusalmada.samplechatfirebase.domain;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

/**
 * Created by vinicius-almada on 28/12/16.
 */

public class InnerRoom {
    private String name;
    private String uuid;
    private List<User> members;
    private Room roomParent;

    public InnerRoom() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUUID() {
        return uuid;
    }

    public void setUUID(String uuid) {
        this.uuid = uuid;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    public Room getRoomParent() {
        return roomParent;
    }

    public void setRoomParent(Room roomParent) {
        this.roomParent = roomParent;
    }

    public void saveOnFirebase() {
        DatabaseReference roomRef = FirebaseDatabase.getInstance().getReference().child("inner_rooms");
        roomRef.child(roomParent.getUUID()).child(uuid).setValue(this);
    }

    public String getMembersName() {
        String str = "";
        for (User u : members) {
            str += u.getName() + ", ";
        }
        return str;
    }

    public boolean containsUser(String uid) {
        for (User u : members) {
            if (u.getUid().equalsIgnoreCase(uid)) {
                return true;
            }
        }
        return false;
    }
}
