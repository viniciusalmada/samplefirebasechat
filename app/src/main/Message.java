/**
 * Classe responsável por guardar e manipular
 * informações da sala (a ser criada pelo usuário);
 */
public class Message {
    private String message;
    private String uidUser;
    private long timestamp;
    private String nameUser;

    /**
     * Opcionalmente pode se criar mais atributos;
     *
     * @param message Nome da sala.
     * @param uidUser  Uid do usuário que enviou a mensagem (estando já dentro da sala)
     * @param timestamp número retornado pelo método getCurrentTimeMilis() para ordenar as mensagens
     * @param nameUser Nome do usuário que enviou a mensagem
     */
    public Room(String message, String uidUser, long timestamp, String nameUser) {
       
    }
}