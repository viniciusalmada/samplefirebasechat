/**
 * Classe responsável por guardar e manipular
 * informações da sala (a ser criada pelo usuário);
 */
public class Room {
    private String name;
    private String uuid;
    private List<User> members;
    private Location location;

    /**
     * Opcionalmente pode se criar mais atributos;
     *
     * @param name Nome da sala.
     * @param uuid  Unique Universal IDentification, string gerada pela classe UUID para identificar a sala de forma única.
     * @param members Lista dos usuários que estão na sala, objetos tipo User.
     * @param location Localização da sala, latitude e longitude;
     */
    public Room(String name, String uuid, List<User> members, Location location) {
        
    }
}